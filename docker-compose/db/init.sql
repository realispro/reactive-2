CREATE TABLE news (
    id int primary key auto_increment,
    headline VARCHAR(255),
    text VARCHAR(255),
    category VARCHAR(32),
    _id      tinyblob
);

INSERT INTO news(headline, text, category) VALUES
                                  ('Elvis is alive!', 'And he lives in Argentina :)', 'ENTERTAINMENT'),
                                  ('Domestos kills coronavirus', 'Professor Trump proves that!', 'SCIENCE'),
                                  ('Poland introduces monarchy', 'The King is Max Kolonko I', 'POLITICS'),
                                  ('Mammoths found on lonely island', 'And they are happy to meet you', 'SCIENCE'),
                                  ('Suprise on Wintar Games!', 'Jamaican bobsled team won competitions!', 'SPORT')

