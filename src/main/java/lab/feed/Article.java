package lab.feed;

import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.net.URI;
import java.time.Instant;

@Document("article")
@Value
public class Article {

    @Id
    private final URI link;
    private final Instant publishedDate;
    private final String title;

}
