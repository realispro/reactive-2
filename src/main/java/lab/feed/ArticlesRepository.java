package lab.feed;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.Tailable;
import reactor.core.publisher.Flux;

import java.net.URI;

public interface ArticlesRepository extends ReactiveMongoRepository<Article, URI> {

    @Tailable
    Flux<Article> getArticlesBy();
}
