package lab.feed.load;

import lab.feed.Article;

import lab.feed.ArticlesRepository;
import lombok.RequiredArgsConstructor;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.function.Function;

@Component
@RequiredArgsConstructor
public class ArticlesStream {

    private static final Logger log = LoggerFactory.getLogger(ArticlesStream.class);

    private final ArticlesRepository articleRepository;
    private final FeedAggregator feedAggregator;

    @PostConstruct
    void checkForNewArticles() {
        feedAggregator
                .articles()
                .filterWhen(onlyNewArticles())
                .flatMap(articleRepository::save)
                .doOnNext(a -> log.debug("Saved {}", a))
                .subscribe(
                        x -> {},
                        e -> log.error("Fatal error when periodically fetching articles", e)
                );
    }

    private Function<Article, Publisher<Boolean>> onlyNewArticles() {
        return article -> articleRepository
                .existsById(article.getLink())
                .doOnNext(exists -> {
                    if (exists) {
                        log.trace("Article {} already found in the database, skipping", article);
                    }
                })
                .map(x -> !x);
    }

}
