package lab.feed.load;

import com.rometools.opml.feed.opml.Opml;
import com.rometools.opml.feed.opml.Outline;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.WireFeedInput;
import lab.feed.Article;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.*;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;


@Component
@Slf4j
@RequiredArgsConstructor
public class FeedAggregator {

    @Value("${feed-file}")
    private String feedFile;
    private final ArticlesReader articlesReader;

    public Flux<Article> articles() {
        return allBlogsStream()
                .flatMap(this::fetchPeriodically);
    }

    private Flux<Article> fetchPeriodically(Outline outline) {
        Duration randDuration = Duration.ofMillis(ThreadLocalRandom.current().nextInt(10_000, 30_000));
        return Flux
                .interval(randDuration)
                .flatMap(i -> fetchEntries(outline.getXmlUrl()))
                .flatMap(this::toArticle);
    }

    private Mono<Article> toArticle(SyndEntry entry) {
        if (entry.getPublishedDate() == null) {
            return Mono.empty();
        }
        return Mono
                .fromCallable(() ->
                        new Article(URI.create(entry.getLink().trim()), entry.getPublishedDate().toInstant(), entry.getTitle())
                )
                .doOnError(e -> log.warn("Unable to create article from {}", entry, e))
                .onErrorComplete();
    }

    private Flux<SyndEntry> fetchEntries(String url) {
        return articlesReader
                .fetch(url)
                .doOnSubscribe(s -> log.info("Fetching entries from {}", url))
                .doOnError(e -> log.warn("Failed to fetch {}", url, e))
                .onErrorComplete();
    }

    private Flux<Outline> allBlogsStream() {
        return Mono
                .fromCallable(this::allFeeds)
                .flatMapIterable(x -> x);
    }

    private List<Outline> allFeeds() throws FeedException, IOException {
        WireFeedInput input = new WireFeedInput();
        try(final InputStream inputStream = FeedAggregator.class.getResourceAsStream(feedFile)) {
            final InputStreamReader streamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            final Reader reader = new BufferedReader(streamReader);
            Opml feed = (Opml) input.build(reader);
            return feed.getOutlines();
        }
    }
}
