package lab.news;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.NotNull;

@Data
@Table
@AllArgsConstructor
@NoArgsConstructor
public class News {

    @Id
    private Long id;

    @NotNull
    private Category category;
    @NotNull
    private String headline;
    @NotNull
    private String text;

    public enum Category {
        POLITICS,
        SPORT,
        ENTERTAINMENT,
        SCIENCE
    }
}
