package lab.news.client;

import lab.news.News;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;

import java.util.Arrays;

@Slf4j
public class NewsClient {

    public static void main(String[] args) {

        String url = "http://localhost:8080";
        /*RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<News[]> response = restTemplate.exchange(
                url + "/reactive/news",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                News[].class);

        Arrays.asList(response.getBody()).forEach(news->log.info(news.toString()));*/

        WebClient webClient = WebClient.builder().build();

        Flux<News> flux = webClient.get()
                .uri(url + "/reactive/news")
                .header("Accept", "text/event-stream")
                .retrieve()
                .bodyToFlux(News.class);

        Disposable disposable = flux.index()
                .doOnNext(t->log.info("{}: {}", t.getT1(), t.getT2()))
                .doOnComplete(()->log.info("news stream completed."))
                .subscribe();

        try {
            Thread.sleep(10*1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        disposable.dispose();


    }
}
