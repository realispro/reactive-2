package lab.news.repository;

import lab.news.News;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface NewsReactiveRepository extends ReactiveCrudRepository<News, Long> {

}
