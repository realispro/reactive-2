package lab.news.repository.r2dbc;

import lab.news.News;
import lab.news.repository.NewsReactiveRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.r2dbc.core.DatabaseClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.function.Function;

// @Repository
@RequiredArgsConstructor
public abstract class NewsR2DBCRepository implements NewsReactiveRepository {

    public static final Function<Map<String, Object>, News> MAPPING_FUNCTION = row -> new News(
            Long.parseLong(row.get("id").toString()),
            News.Category.valueOf(row.get("category").toString()),
            row.get("headline").toString(),
            row.get("text").toString());

    private final DatabaseClient databaseClient;

    @Override
    public Flux<News> findAll() {
        return databaseClient
                .execute(()->"SELECT category, headline, id, text FROM news")
                .fetch()
                .all()
                .map(MAPPING_FUNCTION);
    }

    @Override
    public Mono<News> save(News news) {
        return databaseClient
                .execute(()->"INSERT INTO news(category, headline, text) VALUES(:category, :headline, :text)")
                .bind("category", news.getCategory().name())
                .bind("headline", news.getHeadline())
                .bind("text", news.getText())
                .fetch()
                .one()
                .map(MAPPING_FUNCTION);
    }
}
