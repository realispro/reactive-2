package lab.news.repository.r2dbc;


import io.r2dbc.spi.ConnectionFactories;
import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.ConnectionFactoryOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

import static io.r2dbc.spi.ConnectionFactoryOptions.*;

@Configuration
@EnableR2dbcRepositories(basePackages = "lab.news.repository")
class R2DBCConfiguration extends AbstractR2dbcConfiguration {
    @Bean
    public ConnectionFactory connectionFactory() {
        ConnectionFactoryOptions options = ConnectionFactoryOptions.builder()
                .option(DRIVER, "mysql")
                .option(HOST, "127.0.0.1")
                .option(USER, "root")
                .option(PORT, 3306)  // optional, default 3306
                .option(PASSWORD, "mysql") // optional, default null, null means has no password
                .option(DATABASE, "news")
                .build();

        return ConnectionFactories.get(
                options //"r2dbc:mysql://root:mysql:localhost:3306/lab"
        );
    }

}
