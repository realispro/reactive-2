package lab.news.web;

import lab.news.News;
import lab.news.repository.NewsReactiveRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
@RequestMapping("/reactive")
@Slf4j
@RequiredArgsConstructor
public class NewsReactiveController {

    private final NewsReactiveRepository newsRepository;

    @GetMapping("/news")
    Flux<News> getNews() {
        log.info("about to retrieve all news");

        return newsRepository.findAll()
                        .delayElements(Duration.ofSeconds(2))
                        .doOnCancel(() -> log.warn("stream cancelled by a user."));
    }

    @PostMapping("/news")
    Mono<ResponseEntity<News>> addNews(@RequestBody News news) {
        log.info("adding news {}", news);
        return Mono.defer(() -> newsRepository.save(news))
                .map(n -> ResponseEntity.status(201).body(n))
                .doOnError(t->log.info("problem while saving news", t))
                .onErrorResume(t -> Mono.just(ResponseEntity.badRequest().build()));


    }

    //@DeleteMapping("/news/{id}")


}
