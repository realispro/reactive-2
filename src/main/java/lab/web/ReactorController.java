package lab.web;


import lombok.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

import static org.springframework.http.MediaType.TEXT_EVENT_STREAM_VALUE;


@RestController
class ReactorController {

	private static final Logger log = LoggerFactory.getLogger(ReactorController.class);

	/*private final WebClient webClient;

	ReactorController(WebClient webClient) {
		this.webClient = webClient;
	}
*/
	@GetMapping("/hello")
	Mono<String> hello() {
		return Mono.just(Instant.now())
				.map(instant->instant.toString());
	}

	@GetMapping("/slow")
	Mono<String> slow() {
		return hello()
				.delayElement(Duration.ofSeconds(5));
	}

	@GetMapping(value = "/array")
	Flux<Ping> array() {
		return Flux.range(1, 5)
				.map(i->new Ping(1, Instant.now()));
	}

	@GetMapping(value = "/array-mono")
	Mono<List<Ping>> arrayMono() {
		return array().collectList();
	}

	@GetMapping(value = "/stream")
	Flux<Ping> stream() {
		return array().delayElements(Duration.ofSeconds(2));
	}

	@GetMapping("/error/immediate")
	Flux<String> errorImmediate() {
		return Flux.error(new IllegalArgumentException("let's check"));
	}


	@GetMapping("/proxy")
	Mono<String> exampleProxy() {
		// TODO proxy to example.com
		return null;
	}

	@Value
	static class Ping {
		private final long seqNo;
		private final Instant timestamp;
	}

}



